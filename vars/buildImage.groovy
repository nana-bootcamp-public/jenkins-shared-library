#!/usr/bin/env groovy

def call() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub-cred', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'sudo docker build -t oloruntobi/demo-app:jma-2.0 .'
        sh "echo $PASS | sudo docker login -u $USER --password-stdin"
        sh 'sudo docker push oloruntobi/demo-app:jma-1.0'
    }

}